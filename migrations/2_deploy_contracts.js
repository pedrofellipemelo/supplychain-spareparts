// migrating the appropriate contracts
var AssemblerRole = artifacts.require("./AssemblerRole.sol");
var LogisticRole = artifacts.require("./LogisticRole.sol");
var RetailerRole = artifacts.require("./RetailerRole.sol");
var MechanicRole = artifacts.require("./MechanicRole.sol");
var RegulatorRole = artifacts.require("./RegulatorRole.sol");
var CarManufacturerRole = artifacts.require("./CarManufacturerRole.sol");
var SupplyChain = artifacts.require("./SupplyChain.sol");

module.exports = function(deployer) {
  deployer.deploy(AssemblerRole);
  deployer.deploy(LogisticRole);
  deployer.deploy(RetailerRole);
  deployer.deploy(MechanicRole);
  deployer.deploy(RegulatorRole);
  deployer.deploy(CarManufacturerRole);
  deployer.deploy(SupplyChain);
};
