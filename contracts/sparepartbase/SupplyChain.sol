pragma solidity ^0.4.24;

import "../sparepartaccesscontrol/Roles.sol";
import "../sparepartaccesscontrol/CarManufacturerRole.sol";
import "../sparepartaccesscontrol/LogisticRole.sol";
import "../sparepartaccesscontrol/AssemblerRole.sol";
import "../sparepartaccesscontrol/MechanicRole.sol";
import "../sparepartaccesscontrol/RetailerRole.sol";
import "../sparepartaccesscontrol/RegulatorRole.sol";
import "../sparepartcore/Ownable.sol";

// Define a contract 'Supplychain'

contract SupplyChain is RetailerRole, MechanicRole, AssemblerRole, CarManufacturerRole, LogisticRole, RegulatorRole {

  address owner;

  uint  snb;
  uint  vin;

  mapping (uint => SparePart) spareParts;

  mapping (uint => string[]) sparePartsHistory;

  enum State
  {
    Made,
    Packed,
    ForSale,
    Sold,
    Shipped,
    Received,
    Purchased,
    AddedToCar,
    Replaced
  }

  State constant defaultState = State.Made;

  struct SparePart {
    uint    snb;
    uint    vin;
    address ownerID;
    address originAssemblerID;
    string  assemblerName;
    string  partModel;
    string  partAssemblerLatitude;
    string  partAssemblerLongitude;
    string  partDescription;
    uint    partPrice;
    State   partState;
    address logisticsID;
    address carManufacturerID;
    address retailerID;
    address mechanicID;
  }

  event Made(uint snb);
  event Packed(uint snb);
  event ForSale(uint snb);
  event Sold(uint snb);
  event Shipped(uint snb);
  event Received(uint snb);
//  event Purchased(uint snb);
  event Purchased(uint snb);
  event AddedToCar(uint snb);
  event Replaced(uint snb);


  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

  modifier verifyCaller (address _address) {
    require(msg.sender == _address);
    _;
  }

  modifier paidEnough(uint _price) {
    require(msg.value >= _price);
    _;
  }

  modifier checkValue(uint _snb) {
    _;
    uint _price = spareParts[_snb].partPrice;
    uint amountToReturn = msg.value - _price;
    spareParts[_snb].mechanicID.transfer(amountToReturn);
  }

  modifier made(uint _snb) {
    require(spareParts[_snb].partState == State.Made);
    _;
  }

  modifier packed(uint _snb) {
    require(spareParts[_snb].partState == State.Packed);

    _;
  }

  modifier forSale(uint _snb) {
    require(spareParts[_snb].partState == State.ForSale);

    _;
  }

  modifier sold(uint _snb) {
    require(spareParts[_snb].partState == State.Sold);

    _;
  }

  modifier shipped(uint _snb) {
    require(spareParts[_snb].partState == State.Shipped);
    _;
  }

  modifier received(uint _snb) {
    require(spareParts[_snb].partState == State.Received);
    _;
  }

  modifier purchased(uint _snb) {
    require(spareParts[_snb].partState == State.Purchased);
    _;
  }

  constructor() public payable {
    owner = msg.sender;
    snb = 0;
    vin = 0;
  }

  function kill() public {
    if (msg.sender == owner) {
      selfdestruct(owner);
    }
  }

  function makeSparePart(uint _snb, address _originAssemblerID, string _assemblerName, string _partModel, string  _partAssemblerLatitude, string  _partAssemblerLongitude, string  _partDescription) public

  onlyAssembler
  {
    // Add the new part as part of spareParts
    SparePart memory temp_part = SparePart({
      vin: 0,
      snb:_snb,
      ownerID:_originAssemblerID,
      originAssemblerID:_originAssemblerID,
      assemblerName:_assemblerName,
      partModel: _partModel,
      partAssemblerLatitude:_partAssemblerLatitude,
      partAssemblerLongitude:_partAssemblerLongitude,
      partDescription:_partDescription,
      partState:State.Made,
      partPrice:0,
      logisticsID:0,
      carManufacturerID:0,
      retailerID:0,
      mechanicID:0
      });
      spareParts[_snb] = temp_part;
      spareParts[_snb].partState = State.Made;

      emit Made(_snb);
  }


  function packSparePart(uint _snb) public

  made(_snb)

  onlyAssembler
  {

    spareParts[_snb].partState = State.Packed;

    emit Packed(_snb);
  }


  function sellSparePart(uint _snb, uint _price) public

  packed(_snb)

  onlyLogistic
  {

    spareParts[_snb].partState = State.ForSale;
    spareParts[_snb].partPrice = _price;

    emit ForSale(_snb);
  }

  function buySparePart(uint _snb) public payable

    forSale(_snb)

    paidEnough(spareParts[_snb].partPrice)

    checkValue(_snb)

    onlyLogistic
    {

      spareParts[_snb].partState = State.Sold;

      spareParts[_snb].originAssemblerID.transfer(spareParts[_snb].partPrice);

      emit Sold(_snb);
    }


  function shipSparePart(uint _snb) public

    sold(_snb)

    onlyLogistic
   {
      require(spareParts[_snb].originAssemblerID == msg.sender,"Manufacturers can ship only spareParts by them");

      spareParts[_snb].partState = State.Shipped;

      emit Shipped(_snb);
    }


  function receiveSparePart(uint _snb) public

    shipped(_snb)

    onlyCarManufacturer
    onlyRetailer
    {


    spareParts[_snb].partState = State.Received;

    emit Received(_snb);
  }

  function addSparePartToCar(uint _snb, uint _vin) public

    received(_snb)

    onlyCarManufacturer
    onlyRetailer
    {

      spareParts[_snb].partState = State.Received;
      spareParts[_snb].vin = _vin;
      // Emit the appropriate event
      emit AddedToCar(_snb);
    }

  function puSparePartOnSell(uint _snb, uint _price) public

  packed(_snb)

  onlyRetailer
  {

    spareParts[_snb].partState = State.ForSale;
    spareParts[_snb].partPrice = _price;

    emit ForSale(_snb);
  }


  function purchaseSparePart(uint _snb) public

    received(_snb)

    onlyMechanic
    {

      spareParts[_snb].partState = State.Purchased;

      emit Purchased(_snb);
    }


  function replaceSparePart(uint _snb, uint _vin) public

    received(_snb)

    onlyMechanic
    {


      spareParts[_snb].partState = State.Purchased;
      spareParts[_snb].vin = _vin;

      emit Replaced(_snb);
    }


  function trackHistoryOne(uint _snb) public view returns
  (
    uint    partSNB,
    uint    partVIN,
    address ownerID,
    address originAssemblerID,
    string  assemblerName,
    string  partModel
//    State   partState
  //    string  partAssemblerLatitude,
  //    string  partAssemblerLongitude,
  //    string  partDescription,
  //    uint    partPrice,
//    address logisticsID,
//    address carManufacturerID,
//    address retailerID,
//    address mechanicID

  )

  {

  return
    (
      spareParts[_snb].snb,
      spareParts[_snb].vin,
      spareParts[_snb].ownerID,
      spareParts[_snb].originAssemblerID,
      spareParts[_snb].assemblerName,
      spareParts[_snb].partModel
    //    spareParts[_snb].partState
    //      spareParts[_snb].partAssemblerLatitude,
    //      spareParts[_snb].partAssemblerLongitude,
    //      spareParts[_snb].partDescription,
    //      spareParts[_snb].partPrice,
//      spareParts[_snb].logisticsID,
//      spareParts[_snb].carManufacturerID,
//      spareParts[_snb].retailerID,
//      spareParts[_snb].mechanicID

    );
  }

}
//  // Define a function 'trackHistoryTwo' that fetches the data
//  function trackHistoryTwo(uint _snb) public view returns
//  (
//    // uint    partSKU,
//    // uint    partUPC,
//    // uint    partID,
//    string  originFactoryLongitude,
//    string  partNotes,
//    uint    partPrice,
//    State    partState,
//    address distributorID,
//    address retailerID,
//    address customerID
//    )
//  {
//
//
//
//    return
//    (
//      // spareParts[_snb].sku,
//      // spareParts[_snb].snb,
//      // spareParts[_snb].partID,
//      spareParts[_snb].originFactoryLongitude,
//      spareParts[_snb].partNotes,
//      spareParts[_snb].partPrice,
//      spareParts[_snb].partState,
//      spareParts[_snb].distributorID,
//      spareParts[_snb].retailerID,
//      spareParts[_snb].customerID
//      );
//  }
//
//  // Define a function 'fetchSparePartBufferThree' that fetches the data
//  function fetchSparePartBufferThree(uint _snb) public view returns
//  (
//    uint    partSKU,
//    uint    partUPC,
//    uint    partID
//    )
//  {
//    // Assign values to the 3 parameters
//
//
//    return
//    (
//      spareParts[_snb].sku,
//      spareParts[_snb].snb,
//      spareParts[_snb].partID
//
//      );
//  }


