pragma solidity ^0.4.24;

// Import the library 'Roles'
import "./Roles.sol";

contract LogisticRole {
  using Roles for Roles.Role;

  event LogisticAdded(address indexed account);
  event LogisticRemoved(address indexed account);

  Roles.Role private logistics;

  constructor() public {
    _addLogistic(msg.sender);
  }

  modifier onlyLogistic() {
    require(isLogistic(msg.sender));
    _;
  }

  function isLogistic(address account) public view returns (bool) {
    return Roles.has(logistics, account);
  }

  function addLogistic(address account) public onlyLogistic {
    _addLogistic(account);
  }

  function renounceLogistic() public {
    _removeLogistic(msg.sender);
  }

  function _addLogistic(address account) internal {
    Roles.add(logistics, account);
    emit LogisticAdded(account);
  }

  function _removeLogistic(address account) internal {
    Roles.remove(logistics, account);
    emit LogisticRemoved(account);
  }
}
