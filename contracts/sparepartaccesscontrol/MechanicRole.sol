pragma solidity ^0.4.24;

import "./Roles.sol";
contract MechanicRole {
  using Roles for Roles.Role;

  event MechanicAdded(address indexed account);
  event MechanicRemoved(address indexed account);

  Roles.Role private mechanics;

  constructor() public {
    _addMechanic(msg.sender);
  }

  modifier onlyMechanic() {
    require(isMechanic(msg.sender));
    _;
  }

  function isMechanic(address account) public view returns (bool) {
    return Roles.has(mechanics, account);
  }

  function addMechanic(address account) public onlyMechanic {
    _addMechanic(account);
  }

  function renounceMechanic() public {
    _removeMechanic(msg.sender);
  }

  function _addMechanic(address account) internal {
    Roles.add(mechanics, account);
    emit MechanicAdded(account);
  }
  function _removeMechanic(address account) internal {
    Roles.remove(mechanics, account);
    emit MechanicRemoved(account);
  }
}
