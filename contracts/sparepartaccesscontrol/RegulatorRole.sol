pragma solidity ^0.4.24;

import "./Roles.sol";

contract RegulatorRole{
  using Roles for Roles.Role;

  event RegulatorAdded(address indexed account);
  event RegulatorRemoved(address indexed account);

  Roles.Role private regulators;

  constructor() public {
    _addRegulator(msg.sender);
  }

  modifier onlyRegulator() {
    require(isRegulator(msg.sender));
    _;
  }

  function isRegulator(address account) public view returns (bool) {
    return Roles.has(regulators, account);
  }

  function addRegulator(address account) public onlyRegulator {
    _addRegulator(account);
  }

  function renounceRegulator() public {
    _removeRegulator(msg.sender);
  }

  function _addRegulator(address account) internal {
    Roles.add(regulators, account);
    emit RegulatorAdded(account);
  }

  function _removeRegulator(address account) internal {
    Roles.remove(regulators, account);
    emit RegulatorRemoved(account);
  }
}
