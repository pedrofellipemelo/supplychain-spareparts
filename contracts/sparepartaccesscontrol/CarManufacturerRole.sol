pragma solidity ^0.4.24;

// Import the library 'Roles'
import "./Roles.sol";

contract CarManufacturerRole {
  using Roles for Roles.Role;

  event CarManufacturerAdded(address indexed account);
  event CarManufacturerRemoved(address indexed account);

  Roles.Role private carmanufacturers;
  constructor() public {
    _addCarManufacturer(msg.sender);
  }

  modifier onlyCarManufacturer() {
    require(isCarManufacturer(msg.sender));
    _;
  }

  function isCarManufacturer(address account) public view returns (bool) {
    return Roles.has(carmanufacturers, account);
  }

  function addCarManufacturer(address account) public onlyCarManufacturer {
    _addCarManufacturer(account);
  }

  function renounceCarManufacturer() public {
    _removeCarManufacturer(msg.sender);
  }

  function _addCarManufacturer(address account) internal {
    Roles.add(carmanufacturers, account);
    emit CarManufacturerAdded(account);
  }

  function _removeCarManufacturer(address account) internal {
    Roles.remove(carmanufacturers, account);
    emit CarManufacturerRemoved(account);
  }
}
