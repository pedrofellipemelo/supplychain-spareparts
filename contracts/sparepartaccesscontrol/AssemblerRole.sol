pragma solidity ^0.4.24;

import "./Roles.sol";

contract AssemblerRole{
  using Roles for Roles.Role;

  event AssemblerAdded(address indexed account);
  event AssemblerRemoved(address indexed account);

  Roles.Role private assemblers;

  constructor() public {
    _addAssembler(msg.sender);
  }

  modifier onlyAssembler() {
    require(isAssembler(msg.sender));
    _;
  }

  function isAssembler(address account) public view returns (bool) {
    return Roles.has(assemblers, account);
  }

  function addAssembler(address account) public onlyAssembler {
    _addAssembler(account);
  }

  function renounceAssembler() public {
    _removeAssembler(msg.sender);
  }

  function _addAssembler(address account) internal {
    Roles.add(assemblers, account);
    emit AssemblerAdded(account);
  }

  function _removeAssembler(address account) internal {
    Roles.remove(assemblers, account);
    emit AssemblerRemoved(account);
  }
}