App = {

    snb:0,
    snbPackAssembler: 0,
    snbSellAssembler: 0,
    snbBuyLogistics: 0,
    snbShipLogistics: 0,
    snbReceiveCarmMnufacturer: 0,
    snbAddCarManufacturer: 0,
    snbReceiveRetailer: 0,
    snbSellRetailer: 0,
    snbPurchaseMechanic: 0,
    snbReplaceMechanic: 0,
    snbHistoryOne: 0,

    originAssemblerID: "0x0000000000000000000000000000000000000000",
    assemblerName:null,
    partModel:null,
    partAssemblerLatitude:null,
    partAssemblerLongitude:null,
    partDescription:null,
    web3Provider: null,
    contracts: {},
    emptyAddress: "0x0000000000000000000000000000000000000000",
    sku: 0,
    upc: 0,
    metamaskAccountID: "0x0000000000000000000000000000000000000000",
    ownerID: "0x0000000000000000000000000000000000000000",
    originManufacturerID: "0x0000000000000000000000000000000000000000",
    originFactoryName: null,
    originFactoryInformation: null,
    originFactoryLatitude: null,
    originFactoryLongitude: null,
    productNotes: null,
    productPrice: 0,
    distributorID: "0x0000000000000000000000000000000000000000",
    retailerID: "0x0000000000000000000000000000000000000000",
    customerID: "0x0000000000000000000000000000000000000000",

    init: async function () {
        App.readForm();
        /// Setup access to blockchain
        return await App.initWeb3();
    },

    readForm: function () {

        App.partModel = $("#partModel").val();
        App.snb = $("#snb").val();
        App.snbPackAssembler = $("#snb-pack-assembler").val();
        App.snbSellAssembler = $("#snb-sell-assembler").val();
        App.snbBuyLogistics = $("#snb-buy-logistics").val();
        App.snbShipLogistics = $("#snb-ship-logistics").val();
        App.snbReceiveCarmMnufacturer = $("#snb-receive-carmanufacturer").val();
        App.snbAddCarManufacturer = $("#snb-add-carmanufacturer").val();
        App.snbReceiveRetailer = $("#snb-receive-retailer").val();
        App.snbSellRetailer = $("#snb-sell-retailer").val();
        App.snbPurchaseMechanic = $("#snb-purchase-mechanic").val();
        App.snbReplaceMechanic = $("#snb-replace-mechanic").val();
        App.snbHistoryOne = $("#snb-history-one").val();



        App.originAssemblerID = $("#originAssemblerID").val();
        App.assemblerName = $("#assemblerName").val();
        App.partAssemblerLatitude = $("#partAssemblerLatitude").val();
        App.partAssemblerLongitude = $("#partAssemblerLongitude").val();
        App.partDescription = $("#partDescription").val();
        App.sku = $("#sku").val();
        App.upc = $("#upc").val();
        App.ownerID = $("#ownerID").val();
        App.originManufacturerID = $("#originManufacturerID").val();
        App.originFactoryName = $("#originFactoryName").val();
        App.originFactoryInformation = $("#originFactoryInformation").val();
        App.originFactoryLatitude = $("#originFactoryLatitude").val();
        App.originFactoryLongitude = $("#originFactoryLongitude").val();
        App.productNotes = $("#productNotes").val();
        App.productPrice = $("#productPrice").val();
        App.distributorID = $("#distributorID").val();
        App.retailerID = $("#retailerID").val();
        App.customerID = $("#customerID").val();

        console.log(
            App.sku,
            App.upc,
            App.ownerID, 
            App.originManufacturerID, 
            App.originFactoryName, 
            App.originFactoryInformation, 
            App.originFactoryLatitude, 
            App.originFactoryLongitude, 
            App.productNotes,
            App.productPrice,
            App.distributorID, 
            App.retailerID,
            App.customerID
        );
    },

    initWeb3: async function () {
        /// Find or Inject Web3 Provider
        /// Modern dapp browsers...
        if (window.ethereum) {
            App.web3Provider = window.ethereum;
            try {
                // Request account access
                await window.ethereum.enable();
            } catch (error) {
                // User denied account access...
                console.error("User denied account access")
            }
        }
        // Legacy dapp browsers...
        else if (window.web3) {
            App.web3Provider = window.web3.currentProvider;
        }
        // If no injected web3 instance is detected, fall back to Ganache
        else {
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
        }

        App.getMetaskAccountID();

        return App.initSupplyChain();
    },

    getMetaskAccountID: function () {
        web3 = new Web3(App.web3Provider);

        // Retrieving accounts
        web3.eth.getAccounts(function(err, res) {
            if (err) {
                console.log('Error:',err);
                return;
            }
            console.log('getMetaskID:',res);
            App.metamaskAccountID = res[0];

        })
    },

    initSupplyChain: function () {
        /// Source the truffle compiled smart contracts
        var jsonSupplyChain='../../build/contracts/SupplyChain.json';
        
        /// JSONfy the smart contracts
        $.getJSON(jsonSupplyChain, function(data) {
            console.log('data',data);
            var SupplyChainArtifact = data;
            App.contracts.SupplyChain = TruffleContract(SupplyChainArtifact);
            App.contracts.SupplyChain.setProvider(App.web3Provider);
            
            App.trackHistoryOne();
            App.trackHistoryTwo();
            App.fetchEvents();

        });

        return App.bindEvents();
    },

    bindEvents: function() {
        $(document).on('click', App.handleButtonClick);
    },

    handleButtonClick: async function(event) {
        event.preventDefault();

        App.getMetaskAccountID();

        var processId = parseInt($(event.target).data('id'));
        console.log('processId',processId);

        switch(processId) {
            case 1:
                return await App.makeSparePart(event);
                break;
            case 2:
                return await App.packSparePart(event);
                break;
            case 3:
                return await App.sellSparePart(event);
                break;
            case 4:
                return await App.buySparePart(event);
                break;
            case 5:
                return await App.shipSparePart(event);
                break;
            case 6:
                return await App.receiveSparePart(event);
                break;
            case 7:
                return await App.addSparePartToCar(event);
                break;
            case 8:
                return await App.puSparePartOnSell(event);
                break;
            case 9:
                return await App.purchaseSparePart(event);
                break;
            case 10:
                return await App.replaceSparePart(event);
                break;
            case 12:
                return await App.trackHistoryOne(event);
                break;
            }
    },

    makeSparePart: function(event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        console.log('wqqdqwdwq');
        console.log( App.snb,
            App.metamaskAccountID,
            App.assemblerName,
            App.partModel,
            App.partAssemblerLatitude,
            App.partAssemblerLongitude,
            App.partDescription);

        App.contracts.SupplyChain.deployed().then(function(instance) {
            return instance.makeSparePart(
                App.snb,
                App.metamaskAccountID,
                App.assemblerName,
                App.partModel,
                App.partAssemblerLatitude,
                App.partAssemblerLongitude,
                App.partDescription
            );
        }).then(function(result) {
            // $("#ftc-events").text(result);
            console.log('makeSparePart',result);
        }).catch(function(err) {
            console.log(err.message);
        });
    },

    // processProduct: function (event) {
    //     event.preventDefault();
    //     var processId = parseInt($(event.target).data('id'));
    //
    //     App.contracts.SupplyChain.deployed().then(function(instance) {
    //         return instance.processProduct(App.upc, {from: App.metamaskAccountID});
    //     }).then(function(result) {
    //         $("#ftc-events").text(result);
    //         console.log('processProduct',result);
    //     }).catch(function(err) {
    //         console.log(err.message);
    //     });
    // },
    //

    packSparePart: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function(instance) {
            return instance.packSparePart(App.snbPackAssembler, {from: App.metamaskAccountID});
        }).then(function(result) {
            // $("#ftc-events").text(result);/
            console.log('packProduct',result);
        }).catch(function(err) {
            console.log(err.message);
        });
    },

    sellSparePart: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function(instance) {
            const productPrice = web3.toWei(0.1, "ether");
            console.log('productPrice',productPrice);
            return instance.sellSparePart(App.snbSellAssembler, App.productPrice, {from: App.metamaskAccountID});
        }).then(function(result) {
            $("#ftc-events").text(result);
            console.log('sellSparePart',result);
        }).catch(function(err) {
            console.log(err.message);
        });
    },

    buySparePart: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function(instance) {
            const walletValue = web3.toWei(3, "ether");
            return instance.buySparePart(App.upc, {from: App.metamaskAccountID, value: walletValue});
        }).then(function(result) {
            $("#ftc-events").text(result);
            console.log('buySparePart',result);
        }).catch(function(err) {
            console.log(err.message);
        });
    },

    shipSparePart: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function(instance) {
            return instance.shipSparePart(App.snbShipLogistics, {from: App.metamaskAccountID});
        }).then(function(result) {
            $("#ftc-events").text(result);
            console.log('shipSparePart',result);
        }).catch(function(err) {
            console.log(err.message);
        });
    },

    receiveSparePart: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function(instance) {
            return instance.receiveSparePart(App.snbReceiveCarmMnufacturer, {from: App.metamaskAccountID});
        }).then(function(result) {
            $("#ftc-events").text(result);
            console.log('receiveSparePart',result);
        }).catch(function(err) {
            console.log(err.message);
        });
    },

    addSparePartToCar: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function(instance) {
            return instance.addSparePartToCar(App.upc, {from: App.metamaskAccountID});
        }).then(function(result) {
            $("#ftc-events").text(result);
            console.log('addSparePartToCar',result);
        }).catch(function(err) {
            console.log(err.message);
        });
    },

    trackHistoryOne: function () {
    ///   event.preventDefault();
    ///    var processId = parseInt($(event.target).data('id'));
        App.snbHistoryOne = $('#snb-history-one').val();
        console.log('snbHistoryOne',App.snbHistoryOne);

        App.contracts.SupplyChain.deployed().then(function(instance) {
          return instance.trackHistoryOne(App.snbHistoryOne);
        }).then(function(result) {
          $("#ftc-events").append('<li>' + result + '</li>');
          console.log('trackHistoryOne', result);
        }).catch(function(err) {
          console.log(err.message);
        });
    },

    trackHistoryTwo: function () {
    ///    event.preventDefault();
    ///    var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function(instance) {
          return instance.trackHistoryTwo.call(App.upc);
        }).then(function(result) {
          $("#ftc-events").text('<li>' + log.result + '</li>');

          console.log('trackHistoryTwo', result);
        }).catch(function(err) {
          console.log(err.message);
        });
    },

    fetchEvents: function () {
        if (typeof App.contracts.SupplyChain.currentProvider.sendAsync !== "function") {
            App.contracts.SupplyChain.currentProvider.sendAsync = function () {
                return App.contracts.SupplyChain.currentProvider.send.apply(
                App.contracts.SupplyChain.currentProvider,
                    arguments
              );
            };
        }

        App.contracts.SupplyChain.deployed().then(function(instance) {
        var events = instance.allEvents(function(err, log){
          if (!err)
            $("#ftc-events").append('<li>' + log.event + ' - ' + log.transactionHash + '</li>');
        });
        }).catch(function(err) {
          console.log(err.message);
        });
        
    }
};

$(function () {
    $(window).load(function () {
        App.init();
    });
});
